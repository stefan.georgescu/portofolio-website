if (${login_enabled}) {
  firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          // User is signed in.

          if (window.location.href.endsWith("login.html")) {
              window.location.href = '/index.html';
          }

        } else {
          // No user is signed in.

          if (! window.location.href.endsWith("login.html")) {
              window.location.href = '/login.html';
          }
        }
  });
} else {
  if (window.location.href.endsWith("login.html")) {
    window.location.href = '/index.html';
  }
}




function login(){
  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;

      window.alert("Error : " + errorMessage);

      // ...
  });

}

function logout(){
    firebase.auth().signOut();
}
