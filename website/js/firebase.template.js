// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyAKR-NoKuDkTakORJMnGIj5n1QY4jVeNTQ",
  authDomain: "stefangeorgescu-portofolio.firebaseapp.com",
  databaseURL: "https://stefangeorgescu-portofolio.firebaseio.com",
  projectId: "stefangeorgescu-portofolio",
  storageBucket: "stefangeorgescu-portofolio.appspot.com",
  messagingSenderId: "690779173410",
  appId: "1:690779173410:web:32afc2496428d42f09507b",
  measurementId: "G-4QV0GB5H4R"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.auth();

if (${analytics_enabled}) {
  firebase.analytics();
}
