#!/bin/sh

# Requires one argument: the evnrionment for which this deployment is intended.

set -e

echo "LOG - processing script arguments."

# Process variables
DEPLOY_ENVIRONMENT=$1

echo "LOG - Received following script arguments:"
echo "LOG - DEPLOY_ENVIRONMENT=${DEPLOY_ENVIRONMENT}"

echo "LOG - Processing configuration file."
set -a
. ./buildScripts/conf/${DEPLOY_ENVIRONMENT}.conf
set +a

echo "LOG - Available environment:"
env

echo "LOG - Applying configuration to website files."
envsubst < website/js/auth.template.js > website/js/auth.js
envsubst < website/js/firebase.template.js > website/js/firebase.js

zip -rq website.zip website/
