#!/bin/sh

# Requires one argument: the evnrionment for which this deployment is intended.

set -e

# Process variables
DEPLOY_ENVIRONMENT=$1

echo "LOG - Deploying branch $CI_COMMIT_BRANCH in environment $DEPLOY_ENVIRONMENT."

echo "LOG - Deplpoying to firebase in target $DEPLOY_ENVIRONMENT"

firebase use stefangeorgescu-portofolio --token "$FIREBASE_TOKEN"

firebase target:apply hosting prod $PROD_TARGET
firebase target:apply hosting dev $DEV_TARGET

unzip -o website.zip -d .
firebase deploy --only hosting:$DEPLOY_ENVIRONMENT -m "Gitlab CI/CD Pipe $CI_PIPELINE_ID Build $CI_BUILD_ID" --token "$FIREBASE_TOKEN"
