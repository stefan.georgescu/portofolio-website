
# Portfolio Website
This repository contains the code for my personal website. You can visit it at stefangeorgescu.com.

## Repository Structure
This repository contains a static website, found in the `website` directory, and the shell scripts required to automatically build and deploy, found in `buildScripts`.

## Webiste Template
This website was built with help from [Colorlib](https://colorlib.com). The template used is licenced under CC BY 3.0. The `LICENSE` file present reflects this.

 I fully respected the conditions of the license, having left a reference to the original creator on the page.

## Development and Production Configuration
In order to learn more about CI/CD and how to configure software products, I have created a [test domain](test.stefangeorgescu.com) on which a development version of the website is deployed. The purpose of this was to provide close friends with an easy way of reviewing the website before I released it. Hence, I have created a login page which is displayed only on the Development Configuration. I used Firebase Auth to handle users and Firebase Analytics to measure views.

The script handling the configuration can be found [here](https://gitlab.com/stefan.georgescu/portofolio-website/-/blob/master/buildScripts/prepare_website_version.sh). I used `envsubst` to inject the configuration variables within the JS scripts handling the login redirect.

# Comments
If you have any questions regarding this repository, do not hesitate to [contact me](mailto:contact@stefangeorgescu.com).
